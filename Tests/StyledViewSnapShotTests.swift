//
//  StyledViewSnapShotTests.swift
//  Tests
//
//  Created by Vladislav Ivanov.
//  Copyright © 2020 Vladislav Ivanov. All rights reserved.
//

import XCTest
import StyleableUI
import FBSnapshotTestCase

class StyledViewSnapShotTests: FBSnapshotTestCase {
    
    override func setUp() {
        super.setUp()
        
        recordMode = false
    }
    
    func test_styledView_withoutStyle() {
        let view = StyledView()
        view.frame = UIScreen.main.bounds
        
        FBSnapshotVerifyView(view)
    }
    
    func test_styledView_withStyle() {
        Style.primaryDark.color.background = .green
        
        let view = StyledView(style: Style.primaryDark)
        view.frame = UIScreen.main.bounds
        FBSnapshotVerifyView(view)
    }
    
}
