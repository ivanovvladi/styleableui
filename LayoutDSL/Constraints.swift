//
//  Constraints.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import UIKit

extension UIView {
    
    // MARK: - Leading
    
    @discardableResult 
    public func leadingAnchor(_ anchor: NSLayoutXAxisAnchor,
                              constant: CGFloat = 0,
                              priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: leadingAnchor.constraint(equalTo: anchor, constant: constant),
                   priority: priority)
    }
    
    @discardableResult
    public func leadingAnchor(_ view: UIView,
                              constant: CGFloat = 0,
                              priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: constant),
                   priority: priority)
    }
    
    // MARK: - Trailing
    
    @discardableResult
    public func trailingAnchor(_ anchor: NSLayoutXAxisAnchor,
                               constant: CGFloat = 0,
                               priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: trailingAnchor.constraint(equalTo: anchor, constant: constant),
                   priority: priority)
    }
    
    @discardableResult
    public func trailingAnchor(_ view: UIView,
                               constant: CGFloat = 0,
                               priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: constant),
                   priority: priority)
    }
    
    // MARK: - Top
    
    @discardableResult
    public func topAnchor(_ anchor: NSLayoutYAxisAnchor,
                          constant: CGFloat = 0,
                          priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: topAnchor.constraint(equalTo: anchor, constant: constant),
                   priority: priority)
    }
    
    @discardableResult
    public func topAnchor(_ view: UIView,
                          constant: CGFloat = 0,
                          priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: topAnchor.constraint(equalTo: view.topAnchor, constant: constant),
                   priority: priority)
    }
    
    // MARK: - Bottom
    
    @discardableResult
    public func bottomAnchor(_ anchor: NSLayoutYAxisAnchor,
                             constant: CGFloat = 0,
                             priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: bottomAnchor.constraint(equalTo: anchor, constant: constant),
                   priority: priority)
    }
    
    @discardableResult
    public func bottomAnchor(_ view: UIView,
                             constant: CGFloat = 0,
                             priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: constant),
                   priority: priority)
    }
    
    // MARK: - CenterX
    
    @discardableResult
    public func centerXAnchor(_ anchor: NSLayoutXAxisAnchor,
                              constant: CGFloat = 0,
                              priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: centerXAnchor.constraint(equalTo: anchor, constant: constant),
                   priority: priority)
    }
    
    @discardableResult
    public func centerXAnchor(_ view: UIView,
                              constant: CGFloat = 0,
                              priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: constant),
                   priority: priority)
    }
    
    // MARK: - CenterY
    
    @discardableResult
    public func centerYAnchor(_ anchor: NSLayoutYAxisAnchor,
                              constant: CGFloat = 0,
                              priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: centerYAnchor.constraint(equalTo: anchor, constant: constant),
                   priority: priority)
    }
    
    @discardableResult
    public func centerYAnchor(_ view: UIView,
                              constant: CGFloat = 0,
                              priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: constant),
                   priority: priority)
    }
    
    // MARK: - Center
    
    @discardableResult
    public func centerAnchor(_ view: UIView, priority: UILayoutPriority = .required) -> UIView {
        centerXAnchor(view, priority: priority)
        return centerYAnchor(view, priority: priority)
    }
    
    // MARK: - Height
    
    @discardableResult
    public func heightAnchor(_ constant: CGFloat = 0,
                             priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: heightAnchor.constraint(equalToConstant: constant),
                   priority: priority)
    }
    
    @discardableResult
    public func heightAnchor(_ anchor: NSLayoutDimension,
                             multiplier: CGFloat = 1,
                             priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: heightAnchor.constraint(equalTo: anchor, multiplier: multiplier),
                   priority: priority)
    }
    
    @discardableResult
    public func heightAnchor(greaterThanOrEqualTo view: UIView,
                             multiplier: CGFloat = 1,
                             constant: CGFloat = 0,
                             priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: heightAnchor.constraint(greaterThanOrEqualTo: view.heightAnchor,
                                                       multiplier: multiplier,
                                                       constant: constant),
                   priority: priority)
    }
    
    @discardableResult
    public func heightAnchor(greaterThanOrEqualTo anchor: NSLayoutDimension,
                             multiplier: CGFloat = 1,
                             constant: CGFloat = 0,
                             priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: heightAnchor.constraint(greaterThanOrEqualTo: anchor,
                                                       multiplier: multiplier,
                                                       constant: constant),
                   priority: priority)
    }
    
    @discardableResult
    public func heightAnchor(greaterThanOrEqualTo constant: CGFloat = 0,
                             priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: heightAnchor.constraint(greaterThanOrEqualToConstant: constant),
                   priority: priority)
    }
    
    @discardableResult
    public func heightAnchor(lessThanOrEqualTo constant: CGFloat = 0,
                             priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: heightAnchor.constraint(lessThanOrEqualToConstant: constant),
                   priority: priority)
    }
    
    // MARK: - Width
    
    @discardableResult
    public func widthAnchor(_ constant: CGFloat = 0,
                            priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: widthAnchor.constraint(equalToConstant: constant),
                   priority: priority)
    }
    
    @discardableResult
    public func widthAnchor(_ anchor: NSLayoutDimension,
                            multiplier: CGFloat = 1,
                            priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: widthAnchor.constraint(equalTo: anchor, multiplier: multiplier),
                   priority: priority)
    }
    
    @discardableResult
    public func widthAnchor(greaterThanOrEqualTo view: UIView,
                            multiplier: CGFloat = 1,
                            constant: CGFloat = 0,
                            priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: widthAnchor.constraint(greaterThanOrEqualTo: view.heightAnchor,
                                                      multiplier: multiplier,
                                                      constant: constant),
                   priority: priority)
    }
    
    @discardableResult
    public func widthAnchor(greaterThanOrEqualTo anchor: NSLayoutDimension,
                            multiplier: CGFloat = 1,
                            constant: CGFloat = 0,
                            priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: widthAnchor.constraint(greaterThanOrEqualTo: anchor,
                                                      multiplier: multiplier,
                                                      constant: constant),
                   priority: priority)
    }
    
    @discardableResult
    public func widthAnchor(greaterThanOrEqualTo constant: CGFloat = 0,
                            priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: widthAnchor.constraint(greaterThanOrEqualToConstant: constant),
                   priority: priority)
    }
    
    @discardableResult
    public func widthAnchor(lessThanOrEqualTo constant: CGFloat = 0,
                            priority: UILayoutPriority = .required) -> UIView {
        
        return set(constraint: widthAnchor.constraint(lessThanOrEqualToConstant: constant),
                   priority: priority)
    }
    
    // MARK: - Size
    
    @discardableResult
    public func sizeAnchor(_ size: CGSize = .zero, priority: UILayoutPriority = .required) -> UIView {
        heightAnchor(size.height, priority: priority)
        return widthAnchor(size.width, priority: priority)
    }
    
    @discardableResult
    public func allAnchor(_ view: UIView,
                          constant: CGFloat = 0,
                          priority: UILayoutPriority = .required) -> UIView {
        
        equalVerticalAnchor(view, constant: constant, priority: priority)
        return equalHorizontalAnchor(view, constant: constant, priority: priority)
    }
    
    // MARK: - Horizontal
    
    @discardableResult
    public func equalHorizontalAnchor(_ view: UIView,
                                      constant: CGFloat = 0,
                                      priority: UILayoutPriority = .required) -> UIView {
        
        leadingAnchor(view, constant: constant, priority: priority)
        return trailingAnchor(view, constant: -constant, priority: priority)
    }
    
    // MARK: - Vertical
    
    @discardableResult
    public func equalVerticalAnchor(_ view: UIView,
                                    constant: CGFloat = 0,
                                    priority: UILayoutPriority = .required) -> UIView {
        
        topAnchor(view, constant: constant, priority: priority)
        return bottomAnchor(view, constant: -constant, priority: priority)
    }
    
}

// MARK: - Helpers

extension UIView {
    
    @discardableResult
    public func set(constraint: NSLayoutConstraint,
                    priority: UILayoutPriority = .required) -> UIView {
        
        if translatesAutoresizingMaskIntoConstraints {
            translatesAutoresizingMaskIntoConstraints = false
        }
        
        constraint.priority = priority
        
        if !constraint.isActive {
            constraint.isActive = true
        }
        
        return self
    }
    
}
