//
//  Font.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import UIKit

public protocol FontType {
    
    static var font: UIFont { get set }
    
}

public protocol FontStyleType {
    
    static var primary: FontType.Type { get set }
    static var secondary: FontType.Type { get set }
    static var tertiary: FontType.Type { get set }
    
}

public struct FontStyle: FontStyleType {
    
    private struct Primary: FontType {
        
        static var font: UIFont = .preferredFont(forTextStyle: .title3)
        
    }

    private struct Secondary: FontType {
        
        static var font: UIFont = .preferredFont(forTextStyle: .headline)
        
    }

    private struct Tertiary: FontType {
        
        static var font: UIFont = .preferredFont(forTextStyle: .body)
        
    }

    public static var primary: FontType.Type = Primary.self
    public static var secondary: FontType.Type = Secondary.self
    public static var tertiary: FontType.Type = Tertiary.self
    
}

extension UIFont: FontStyleType {
    
    public static var primary: FontType.Type = FontStyle.primary
    public static var secondary: FontType.Type = FontStyle.secondary
    public static var tertiary: FontType.Type = FontStyle.tertiary
    
}
