//
//  StyledSelectableLabel.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import UIKit

open class StyledSelectableLabel: StyledLabel, Selectable {
    
    open var isSelected: Bool = false {
        willSet {
            if newValue, let selected = selectedStyle {
                apply(style: selected)
            } else {
                apply(style: style)
            }
        }
    }
    
    private var style: Styleable.Type?
    private var selectedStyle: Styleable.Type?
    
    public init(style: Styleable.Type? = nil, selectedStyle: Styleable.Type? = nil) {
        self.style = style
        self.selectedStyle = selectedStyle
        
        super.init(style: style)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func apply(style: Styleable.Type, selectedStyle: Styleable.Type) {
        self.style = style
        
        apply(style: style)
        
        self.selectedStyle = selectedStyle
    }
    
}
