//
//  UITableView+Extensions.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import UIKit

extension UITableView {
    
    public func registerCell<T>(_ cellClass: T.Type) where T: Reusable {
        register(cellClass.self, forCellReuseIdentifier: T.reuseID)
    }
    
    public func dequeueReusableCell<T: UITableViewCell>(for path: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseID, for: path) as? T else {
            fatalError("Couldn't cast UITableViewCell to the expected type")
        }
        
        return cell
    }
    
    public func dequeueReusableCell<T: UITableViewCell>(for path: IndexPath) -> T? {
        return dequeueReusableCell(withIdentifier: T.reuseID, for: path) as? T
    }

    public func registerView<T>(_ viewClass: T.Type) where T: Reusable {
        register(viewClass.self, forHeaderFooterViewReuseIdentifier: T.reuseID)
    }
    
    public func dequeueReusableView<T: UITableViewHeaderFooterView>() -> T {
        guard let view = dequeueReusableHeaderFooterView(withIdentifier: T.reuseID) as? T else {
            fatalError("Couldn't cast UITableViewHeaderFooterView to the expected type")
        }
        
        return view
    }
    
    public func dequeueReusableView<T: UITableViewHeaderFooterView>() -> T? {
        return dequeueReusableHeaderFooterView(withIdentifier: T.reuseID) as? T
    }
    
}
