//
//  Selectable.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import Foundation

public protocol Selectable: class {
    
    var isSelected: Bool { get set }
    
}
