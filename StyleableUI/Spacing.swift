//
//  Spacing.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import UIKit

public protocol SpacingType {
    
    static var vertical: CGFloat { get set }
    static var horizontal: CGFloat { get set }
    static var verticalBetween: CGFloat { get set }
    static var horizontalBetween: CGFloat { get set }
    
}

public protocol SpacingStyleType {
    
    static var primary: SpacingType.Type { get set }
    static var zero: SpacingType.Type { get set }

}

public struct SpacingStyle: SpacingStyleType {
    
    private struct Primary: SpacingType {
        
        static var vertical: CGFloat = 8
        static var horizontal: CGFloat = 16
        static var verticalBetween: CGFloat = 8
        static var horizontalBetween: CGFloat = 16
        
    }
    
    private struct Zero: SpacingType {
        
        static var vertical: CGFloat = 0
        static var horizontal: CGFloat = 0
        static var verticalBetween: CGFloat = 0
        static var horizontalBetween: CGFloat = 0
        
    }
    
    public static var primary: SpacingType.Type = Primary.self
    public static var zero: SpacingType.Type = Zero.self

}

extension CGFloat: SpacingStyleType {
    
    public static var primary: SpacingType.Type = SpacingStyle.primary
    public static var zero: SpacingType.Type = SpacingStyle.zero
    
}
