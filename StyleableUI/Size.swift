//
//  Size.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import UIKit

public protocol SizeType {
    
    static var iconSize: CGSize { get set }
    
}

public protocol SizeStyleType {
    
    static var primary: SizeType.Type { get set }
    static var secondary: SizeType.Type { get set }
    static var tertiary: SizeType.Type { get set }
    
}

public struct SizeStyle: SizeStyleType {
    
    private struct Primary: SizeType {
        
        static var iconSize: CGSize = .init(width: 80, height: 80)
        
    }

    private struct Secondary: SizeType {
        
        static var iconSize: CGSize = .init(width: 40, height: 40)
        
    }

    private struct Tertiary: SizeType {
        
        static var iconSize: CGSize = .init(width: 20, height: 20)

    }

    public static var primary: SizeType.Type = Primary.self
    public static var secondary: SizeType.Type = Secondary.self
    public static var tertiary: SizeType.Type = Tertiary.self
    
}

extension CGSize: SizeStyleType {
    
    public static var primary: SizeType.Type = SizeStyle.primary
    public static var secondary: SizeType.Type = SizeStyle.secondary
    public static var tertiary: SizeType.Type = SizeStyle.tertiary
    
}
