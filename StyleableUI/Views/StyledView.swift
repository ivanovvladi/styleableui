//
//  StyledView.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import UIKit

open class StyledView: UIView {
    
    public init(style: Styleable.Type? = nil) {
        super.init(frame: .zero)
        
        viewDidLoad()
        apply(style: style)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func viewDidLoad() {
        
    }
    
    open func apply(style: Styleable.Type? = nil) {
        backgroundColor = style?.color.background
    }
    
}
