//
//  StyledSelectableView.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import UIKit

public protocol StyledSelectableViewDelegate: class {
    
    func didTapOnStyledSelectableView()
    
}

open class StyledSelectableView: StyledView, Selectable {
    
    // MARK: - Properties
    
    private var style: Styleable.Type?
    private var selectedStyle: Styleable.Type?
    private weak var delegate: StyledSelectableViewDelegate?
    
    open var isSelected: Bool = false {
        willSet {
            if newValue {
                apply(style: style)
            } else {
                apply(style: selectedStyle)
            }
        }
    }
    
    // MARK: - Life cycle
    
    public init(style: Styleable.Type? = nil, selectedStyle: Styleable.Type? = nil) {
        self.style = style
        self.selectedStyle = selectedStyle
        
        super.init(style: style)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        setupAction()
    }
    
    // MARK: - Configure
    
    open func configure(delegate: StyledSelectableViewDelegate?) {
        self.delegate = delegate
    }
    
    open func apply(style: Styleable.Type, selectedStyle: Styleable.Type) {
        self.style = style
        
        apply(style: style)
        
        self.selectedStyle = selectedStyle
    }
    
    // MARK: - Actions
    
    private func setupAction() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                          action: #selector(didTapOnStyledSelectableView))
        tapGestureRecognizer.delegate = self
        
        addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc
    private func didTapOnStyledSelectableView() {
        delegate?.didTapOnStyledSelectableView()
    }
    
}

// MARK: - UIGestureRecognizerDelegate

extension StyledSelectableView: UIGestureRecognizerDelegate {
    
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        isSelected = true
    }
    
    override open func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        isSelected = false
    }
    
    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        isSelected = false
    }
    
}
