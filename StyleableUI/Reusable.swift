//
//  Reusable.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import Foundation

public protocol Reusable: class {
    
    static var reuseID: String { get }
    
}

public extension Reusable {
    
    static var reuseID: String {
        return "\(type(of: self)):\(ObjectIdentifier(self))"
    }
    
}

extension NSObject: Reusable {}
