//
//  Style.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import UIKit

public protocol Styleable {
    
    static var color: ColorType.Type { get set }
    static var font: FontType.Type { get set }
    static var size: SizeType.Type { get set }
    static var spacing: SpacingType.Type { get set }
    
}

public protocol StyleType {
    
    static var primaryDark: Styleable.Type { get set }
    static var primaryDarkNeutral: Styleable.Type { get set }
    static var primaryLight: Styleable.Type { get set }
    static var primaryLightNeutral: Styleable.Type { get set }
    static var secondaryDark: Styleable.Type { get set }
    static var secondaryDarkNeutral: Styleable.Type { get set }
    static var secondaryLight: Styleable.Type { get set }
    static var secondaryLightNeutral: Styleable.Type { get set }
    static var tertiaryDark: Styleable.Type { get set }
    static var tertiaryDarkNeutral: Styleable.Type { get set }
    static var tertiaryLight: Styleable.Type { get set }
    static var tertiaryLightNeutral: Styleable.Type { get set }
    
}

public struct Style: StyleType {
    
    private struct PrimaryDark: Styleable {
        
        static var color: ColorType.Type = UIColor.dark.self
        static var font: FontType.Type = UIFont.primary.self
        static var size: SizeType.Type = CGSize.primary.self
        static var spacing: SpacingType.Type = CGFloat.primary.self
        
    }

    private struct PrimaryDarkNeutral: Styleable {
        
        static var color: ColorType.Type = UIColor.darkNeutral.self
        static var font: FontType.Type = UIFont.primary.self
        static var size: SizeType.Type = CGSize.primary.self
        static var spacing: SpacingType.Type = CGFloat.primary.self
        
    }

    private struct PrimaryLight: Styleable {
        
        static var color: ColorType.Type = UIColor.light.self
        static var font: FontType.Type = UIFont.primary.self
        static var size: SizeType.Type = CGSize.primary.self
        static var spacing: SpacingType.Type = CGFloat.primary.self
        
    }

    private struct PrimaryLightNeutral: Styleable {
        
        static var color: ColorType.Type = UIColor.lightNeutral.self
        static var font: FontType.Type = UIFont.primary.self
        static var size: SizeType.Type = CGSize.primary.self
        static var spacing: SpacingType.Type = CGFloat.primary.self
        
    }

    private struct SecondaryDark: Styleable {
        
        static var color: ColorType.Type = UIColor.dark.self
        static var font: FontType.Type = UIFont.secondary.self
        static var size: SizeType.Type = CGSize.secondary.self
        static var spacing: SpacingType.Type = CGFloat.primary.self
        
    }

    private struct SecondaryDarkNeutral: Styleable {
        
        static var color: ColorType.Type = UIColor.darkNeutral.self
        static var font: FontType.Type = UIFont.secondary.self
        static var size: SizeType.Type = CGSize.secondary.self
        static var spacing: SpacingType.Type = CGFloat.primary.self
        
    }

    private struct SecondaryLight: Styleable {
        
        static var color: ColorType.Type = UIColor.light.self
        static var font: FontType.Type = UIFont.secondary.self
        static var size: SizeType.Type = CGSize.secondary.self
        static var spacing: SpacingType.Type = CGFloat.primary.self
        
    }

    private struct SecondaryLightNeutral: Styleable {
        
        static var color: ColorType.Type = UIColor.lightNeutral.self
        static var font: FontType.Type = UIFont.secondary.self
        static var size: SizeType.Type = CGSize.secondary.self
        static var spacing: SpacingType.Type = CGFloat.primary.self
        
    }

    private struct TertiaryDark: Styleable {
        
        static var color: ColorType.Type = UIColor.dark.self
        static var font: FontType.Type = UIFont.tertiary.self
        static var size: SizeType.Type = CGSize.tertiary.self
        static var spacing: SpacingType.Type = CGFloat.primary.self
        
    }

    private struct TertiaryDarkNeutral: Styleable {
        
        static var color: ColorType.Type = UIColor.darkNeutral.self
        static var font: FontType.Type = UIFont.tertiary.self
        static var size: SizeType.Type = CGSize.tertiary.self
        static var spacing: SpacingType.Type = CGFloat.primary.self
        
    }

    private struct TertiaryLight: Styleable {
        
        static var color: ColorType.Type = UIColor.light.self
        static var font: FontType.Type = UIFont.tertiary.self
        static var size: SizeType.Type = CGSize.tertiary.self
        static var spacing: SpacingType.Type = CGFloat.primary.self
        
    }

    private struct TertiaryLightNeutral: Styleable {
        
        static var color: ColorType.Type = UIColor.lightNeutral.self
        static var font: FontType.Type = UIFont.tertiary.self
        static var size: SizeType.Type = CGSize.tertiary.self
        static var spacing: SpacingType.Type = CGFloat.primary.self
        
    }
    
    public static var primaryDark: Styleable.Type = PrimaryDark.self
    public static var primaryDarkNeutral: Styleable.Type = PrimaryDarkNeutral.self
    public static var primaryLight: Styleable.Type = PrimaryLight.self
    public static var primaryLightNeutral: Styleable.Type = PrimaryLightNeutral.self
    public static var secondaryDark: Styleable.Type = SecondaryDark.self
    public static var secondaryDarkNeutral: Styleable.Type = SecondaryDarkNeutral.self
    public static var secondaryLight: Styleable.Type = SecondaryLight.self
    public static var secondaryLightNeutral: Styleable.Type = SecondaryLightNeutral.self
    public static var tertiaryDark: Styleable.Type = TertiaryDark.self
    public static var tertiaryDarkNeutral: Styleable.Type = TertiaryDarkNeutral.self
    public static var tertiaryLight: Styleable.Type = TertiaryLight.self
    public static var tertiaryLightNeutral: Styleable.Type = TertiaryLightNeutral.self
    
}
