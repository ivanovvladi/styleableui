//
//  Color.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import UIKit

extension UIColor {
    
    public convenience init(r: Double, g: Double, b: Double, a: Double) {
        let red = CGFloat(r / 255.0)
        let green = CGFloat(g / 255.0)
        let blue = CGFloat(b / 255.0)
        let alpha = CGFloat(a)
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
}

public protocol ColorType {
    
    static var foreground: UIColor { get set }
    static var background: UIColor { get set }
    
}

public protocol ColorStyleType {
    
    static var dark: ColorType.Type { get set }
    static var darkNeutral: ColorType.Type { get set }
    static var light: ColorType.Type { get set }
    static var lightNeutral: ColorType.Type { get set }
    
}

public struct ColorStyle: ColorStyleType {
    
    private struct Dark: ColorType {
        
        static var foreground: UIColor = .init(r: 25, g: 28, b: 31, a: 1)
        static var background: UIColor = .white
        
    }

    private struct DarkNeutral: ColorType {
        
        static var foreground: UIColor = .init(r: 139, g: 149, b: 158, a: 1)
        static var background: UIColor = .white
        
    }

    private struct Light: ColorType {
        
        static var foreground: UIColor = .init(r: 0, g: 117, b: 235, a: 1)
        static var background: UIColor = .white
        
    }

    private struct LightNeutral: ColorType {
        
        static var foreground: UIColor = .init(r: 0, g: 117, b: 235, a: 0.75)
        static var background: UIColor = .white
        
    }

    public static var dark: ColorType.Type = Dark.self
    public static var darkNeutral: ColorType.Type = DarkNeutral.self
    public static var light: ColorType.Type = Light.self
    public static var lightNeutral: ColorType.Type = LightNeutral.self
    
}

extension UIColor: ColorStyleType {
    
    public static var dark: ColorType.Type = ColorStyle.dark
    public static var darkNeutral: ColorType.Type = ColorStyle.darkNeutral
    public static var light: ColorType.Type = ColorStyle.light
    public static var lightNeutral: ColorType.Type = ColorStyle.lightNeutral
    
}
