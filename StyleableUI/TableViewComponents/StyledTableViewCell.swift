//
//  StyledTableViewCell.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import UIKit

open class StyledTableViewCell: UITableViewCell {
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        viewDidLoad()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func viewDidLoad() {

    }
    
    open func apply(style: Styleable.Type? = nil) {
        backgroundColor = style?.color.background
        contentView.backgroundColor = style?.color.background
    }
    
}
