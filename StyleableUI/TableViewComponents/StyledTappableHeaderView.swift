//
//  StyledTappableHeaderView.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import UIKit

public protocol StyledTappableHeaderViewDelegate: class {
    
    func didTapOnStyledTappableHeaderView()
    
}

open class StyledTappableHeaderView: StyledTableViewHeaderFooterView, Selectable {
    
    // MARK: - Properties
    
    private weak var delegate: StyledTappableHeaderViewDelegate?
    
    open var isSelected: Bool = false
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        isSelected = false
        setupAction()
    }
    
    override open func prepareForReuse() {
        super.prepareForReuse()
        
        isSelected = false
        delegate = nil
    }
    
    // MARK: - Configure
    
    open func configure(delegate: StyledTappableHeaderViewDelegate?) {
        self.delegate = delegate
    }
    
    // MARK: - Actions
    
    private func setupAction() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                          action: #selector(didTapOnStyledTappableHeaderView))
        
        contentView.addGestureRecognizer(tapGestureRecognizer)
        
        tapGestureRecognizer.delegate = self
    }
    
    @objc
    private func didTapOnStyledTappableHeaderView() {
        delegate?.didTapOnStyledTappableHeaderView()
    }
    
}

// MARK: - UIGestureRecognizerDelegate

extension StyledTappableHeaderView: UIGestureRecognizerDelegate {
            
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        isSelected = true
    }
    
    override open func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        isSelected = false
    }
    
    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        isSelected = false
    }
    
}
