//
//  StyledTableViewHeaderFooterView.swift
//  StyleableUI
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import UIKit

open class StyledTableViewHeaderFooterView: UITableViewHeaderFooterView {
    
    override public init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        viewDidLoad()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func viewDidLoad() {
        
    }
    
    open func apply(style: Styleable.Type? = nil) {
        contentView.backgroundColor = style?.color.background
    }
    
}
